<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Agenda UAç</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <script src="https://kit.fontawesome.com/d7c5e79122.js" crossorigin="anonymous"></script>

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  
  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

</head>

<body>

<?php 
if(!isset($_SESSION['id_user']))  {$_SESSION['id_user']=1;}
if(!isset($_SESSION['username']))   {$_SESSION['username']='Anonimo';}
if(!isset($_SESSION['id_role']))  {$_SESSION['id_role']=0;} 
if(!isset($_SESSION['role']))  {$_SESSION['role']="Guest";} 
if(!isset($_SESSION['email']))  {$_SESSION['email']="guest@guestmail.com";}

?>
        

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">
      <div id="logo" class="pull-left">
       
        <a href="#" class="scrollto"><img src="assets/img/logo_2.png" alt="" title=""></a>
      </div>
      <div>
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#intro">Home</a></li>
          <li><a href="#trending">Trending</a></li>
          <li><a href="#schedule">Monthly Highlights</a></li>
          <li><a href="#recent">Recent Events</a></li>
          <li><a href="#post">Recent Posts</a></li>
          <li><a href="#contact">Contact</a></li> 
          
          <?php if($_SESSION['id_role']==-1) 
                    {
                        echo '<li><a href="#admin">Admin</a></li>';
                    }
            ?>
          <?php if($_SESSION['id_role']==-1 || $_SESSION['id_role']==1 ) 
                    {
                        echo '<li><a href="../backend/edit/">Show</a></li>';
                    }
            ?>
          <li>
          <form action='backend/database/public/search/search.php' method="post">
                <div class="search-box">
                  <input name="keyword" type="text" class="search-txt" placeholder="Type to search"> 
                  <a class="search-btn" name="glass"><i class="fas fa-search"></i></a>
                </div>
          </form>
          </li>
        <?php 
        if($_SESSION['id_role']==0)
        {
            require 'backend/login_form.php';
        } else {
            echo "<li><a>@".$_SESSION['username'].' '.$_SESSION['role']."</a></li>";
            echo '<li><a href="backend/database/user/logout.php">Logout</a></li>';
        }
        ?>

          </nav><!-- #nav-menu-container -->
      </div>
    </div>
  </header><!-- End Header -->
  <!-- ======= Intro Section ======= -->
  <section id="intro">
    <div class="intro-container wow fadeIn">
      <h1 class="mb-4 pb-0">Agenda | UAç</h1>
      <p class="mb-4 pb-0">Find all the information every event related to The University</p> <br> <br> 
      <a href="#contact" class="about-btn scrollto">More Info</a>
    </div>
  </section><!-- End Intro Section -->

  <main id="main">

    <?php

    if($_SESSION['id_role']==-1)
    {
        require 'backend/admin_form.php'; 
    }

    require 'backend/trending_form.php';

    require 'backend/schedule_form.php';

    require 'backend/most_recent_form.php'; 
    
    require 'backend/most_recent_posts_form.php';
    
    require 'backend/gallery_form.php';

    ?>
    
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="section-bg wow fadeInUp">

      <div class="container">

        <div class="section-header">
          <h2>Contact Us</h2>
          <p>Contacts of University of Azores</p>
        </div>

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Address</h3>
              <address> R. da Mãe de Deus, 9500-321 Ponta Delgada</address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Phone Number</h3>
              <p><a href="tel:+351 296 650 000"> 296 650 000</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a href="mailto:reitoria.gabinete@uac.pt">reitoria.gabinete@uac.pt</a></p>
            </div>
          </div>

        </div>

        <div class="form">
          <form action="backend/database/public/insert_message.php" method="post" role="form" class="php-email-form">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name_contact" class="form-control" id="name_contact" placeholder="Nome" data-rule="minlen:2" data-msg="Insira no minimo 2 caracteres" />
                <div class="validate"></div>
              </div>
              <?php ?>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email_contact" id="email_contact" placeholder="Email" data-rule="email" data-msg="Insira um email valido!" />
                <div class="validate"></div>
              </div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="title_contact" id="title_contact" placeholder="Assunto" data-rule="minlen:4" data-msg="Insira no minimo 4 caracteres" />
              <div class="validate"></div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="text_contact" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
              <div class="validate"></div>
            </div>
            <div class="form-group">
              <input type="hidden" name="id_user_contact" <?php echo 'value="'. $_SESSION["id_user"].'"' ?> ></textarea>
            </div>
            <div class="mb-3">
              <div class="loading">Loading</div>
              <div class="error-message"></div>
              <div class="sent-message">Mensagem enviada! Obrigado!</div>
            </div>
            <div class="text-center"><button type="submit">Send Message</button></div>
          </form>
        </div>

      </div>
    </section><!-- End Contact Section -->


</main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>FOLLOW US ON SOCIAL MEDIA</h4>
             <div class="social-links">
             <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>
              
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>NAVIGATE</h4>
            <ul>
              <li><i class="fa fa-angle-right"></i> <a href="#credits">About us</a></li>
              <li><i class="fa fa-angle-right"></i> <a href="#contact">Contact us</a></li>
              <li><i class="fa fa-angle-right"></i> <a href="#">Support</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
                <img src="assets/img/logo1.png" alt="Speaker 1" class="img-fluid">
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>UAç</strong>. All Rights Reserved
      </div>
      <div class="credits">
        Developed by Group 4
        with <a style="size:10px; color:cyan;" href="https://bootstrapmade.com/theevent-conference-event-bootstrap-template/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/superfish/superfish.min.js"></script>
  <script src="assets/vendor/hoverIntent/hoverIntent.js"></script>


  
  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

  <script type="text/javascript">
  $(document).ready(function(){
      var arrow = $(".arrow-up");
      var form = $(".login-form");
      var status = false;
      $("#Login").click(function(event){
          event.preventDefault();
          if (status == false){
             arrow.fadeIn();
             form.fadeIn();
             status == true;
          }else{
            arrow.fadeOut();
            form.fadeOut();
            status == false;
          }
          
      })
  })
  

  </script>
</body>

</html>
