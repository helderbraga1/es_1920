-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 09, 2020 at 08:43 AM
-- Server version: 10.3.22-MariaDB-0+deb10u1
-- PHP Version: 7.3.14-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uac_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `Category`
--

CREATE TABLE `Category` (
  `id` int(11) NOT NULL,
  `category` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16le;

--
-- Dumping data for table `Category`
--

INSERT INTO `Category` (`id`, `category`) VALUES
(1, 'Uncategorized'),
(2, 'Codeweek'),
(3, 'Grande League 2020');

-- --------------------------------------------------------

--
-- Table structure for table `Event`
--

CREATE TABLE `Event` (
  `id` int(11) NOT NULL,
  `title` varchar(64) NOT NULL,
  `text` varchar(5096) NOT NULL,
  `created_date` date NOT NULL DEFAULT current_timestamp(),
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `id_category` int(11) NOT NULL,
  `visible` int(1) NOT NULL DEFAULT 1,
  `img_path` varchar(2048) NOT NULL DEFAULT '../no_image_available.jpg',
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16le;

--
-- Dumping data for table `Event`
--

INSERT INTO `Event` (`id`, `title`, `text`, `created_date`, `start_date`, `end_date`, `id_category`, `visible`, `img_path`, `id_user`) VALUES
(3, 'Le Requiem', 'Bienvenuti!', '2020-05-01', '2020-05-06', '2020-05-29', 1, 1, 'le_requiem.jpg', 1),
(4, 'Codeweek 2021', 'Gosta de robotica? Interessado em informatica? Venha ter conosco esta semana e participe na Codeweek 2020!', '2020-05-09', '2020-05-10', '2020-05-12', 2, 1, 'codeweek2020.jpg', 2),
(5, 'Grande League 2020', 'Aqui competem os melhores jogadores da ilha! Achas-te à altura? Então participa esta semana!', '2020-05-03', '2020-05-04', '2020-05-21', 3, 1, 'grande_league.jpg', 2),
(7, 'BIG >> small       <br>>>> [compression] <<<', 'In the era of Big Data, compression is key!', '2020-05-04', '2020-05-06', '2020-05-22', 1, 1, 'bigsmall.jpg', 3),
(9, 'ads', 'Bienvenuti!', '2020-05-01', '2020-05-06', '2020-05-29', 1, 1, 'le_requiem.jpg', 1),
(12, 'Seminario', 'Venha ao nosso auditorio e aprenda algo novo sobre a historia da UAc', '2020-05-14', '2020-05-15', '2020-05-16', 1, 1, 'seminar.jpg', 2),
(14, 'Codeweek 2020', 'Gosta de robotica? Interessado em informatica? Venha ter conosco esta semana e participe na Codeweek 2020!', '2020-05-09', '2020-05-10', '2020-05-12', 2, 1, 'codeweek2020.jpg', 2),
(15, 'Grande League 2020', 'Aqui competem os melhores jogadores da ilha! Achas-te à altura? Então participa esta semana!', '2020-05-03', '2020-05-04', '2020-05-21', 3, 1, 'grande_league.jpg', 2),
(16, 'Seminario', 'Venha ao nosso auditorio e aprenda algo novo sobre a historia da UAc', '2020-05-14', '2020-05-15', '2020-05-16', 1, 1, 'seminar.jpg', 2),
(18, 'Le Requiem', 'Bienvenuti!', '2020-05-01', '2020-05-06', '2020-05-29', 1, 1, 'le_requiem.jpg', 1),
(19, 'Seminario', 'Venha ao nosso auditorio e aprenda algo novo sobre a historia da UAc', '2020-05-14', '2020-05-15', '2020-05-16', 1, 1, 'seminar.jpg', 2),
(23, 'Teste', '12312312', '2020-05-08', '2020-05-20', '2020-05-29', 0, 1, '', 19);

-- --------------------------------------------------------

--
-- Table structure for table `Event_Post`
--

CREATE TABLE `Event_Post` (
  `id_event` int(11) NOT NULL,
  `id_post` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16le;

--
-- Dumping data for table `Event_Post`
--

INSERT INTO `Event_Post` (`id_event`, `id_post`) VALUES
(1, 7),
(2, 1),
(3, 2),
(4, 2),
(4, 6),
(4, 8),
(5, 2),
(5, 3),
(5, 8),
(6, 4);

-- --------------------------------------------------------

--
-- Table structure for table `Event_Tag`
--

CREATE TABLE `Event_Tag` (
  `event_id` int(11) NOT NULL DEFAULT 0,
  `tag_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf16le;

--
-- Dumping data for table `Event_Tag`
--

INSERT INTO `Event_Tag` (`event_id`, `tag_id`) VALUES
(4, 3),
(4, 4);

-- --------------------------------------------------------

--
-- Table structure for table `Message`
--

CREATE TABLE `Message` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(64) NOT NULL,
  `title` varchar(64) NOT NULL,
  `text` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `id_user` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf16le COMMENT='Contains the "Contact US" page sent forms';

--
-- Dumping data for table `Message`
--

INSERT INTO `Message` (`id`, `name`, `email`, `title`, `text`, `created_date`, `id_user`) VALUES
(3, 'Zacarias', 'zacarias@zacariasmail.com', 'Zacarias Shopping', 'Queremos publicitar no seu site, contacte-nos!', '2020-05-06 06:34:23', 1),
(4, 'Zacarias', 'zacarias@zacariasmail.com', 'Zacarias Shopping', 'Queremos publicitar no seu site, contacte-nos! Bom dia para si!', '2020-05-06 06:37:54', 1),
(5, 'Zacarias', 'zacarias@zacariasmail.com', 'Zacarias Shopping', 'Queremos publicitar no seu site, contacte-nos! Bom dia para si!', '2020-05-06 07:16:25', 1),
(6, 'Zacarias', 'zacarias@zacariasmail.com', 'Zacarias Shopping', 'asd', '2020-05-06 07:26:43', 1),
(7, 'Zacarias', 'zacarias@zacariasmail.com', 'Zacarias Shopping', 'asddd', '2020-05-06 07:27:20', 1),
(8, 'aa', 'aaaa@aaa.aaa', 'ssaa', 'dddd', '2020-05-06 07:34:41', 1),
(9, 'Zacarias', 'zacarias@zacariasmail.com', 'Zacarias Shopping', 'asd d asdasdads', '2020-05-06 07:36:21', 1),
(10, 'Polibius', 'polibius@polibiusmail.com', 'Polibius', 'Polibius', '2020-05-07 08:54:36', 2),
(11, 'tester', 'tester@tester.pt', 'teste', 'testeeee', '2020-05-07 19:41:24', 2),
(12, 'marim', 'marim@marim.pt', 'teste do marim', 'mariiiiiiiiiiim ', '2020-05-07 19:43:09', 17),
(13, 'testando', 'teste@gmail.com', 'teste', 'Isso é um teste', '2020-05-08 17:25:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `Post`
--

CREATE TABLE `Post` (
  `id` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `text` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `img_path` varchar(512) NOT NULL DEFAULT '../no_image_available.jpg',
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16le;

--
-- Dumping data for table `Post`
--

INSERT INTO `Post` (`id`, `title`, `text`, `created_date`, `img_path`, `id_user`) VALUES
(1, 'asdvv', 'dsvnbva', '2020-05-06 11:36:07', '01.jpg', 2),
(2, 'zvxcvxv', 'xcvxcvx', '2020-05-06 11:36:07', '04.jpeg', 1),
(3, 'Test', 'Testing', '2020-05-06 11:36:07', '01.jpg', 1),
(4, 'ABC', 'ABBCDF', '2020-05-06 11:36:07', '05.png', 2),
(5, 'ABC', 'ABBCDF', '2020-05-06 11:36:07', '06.jpg', 2),
(6, 'ds', 'asd', '2020-05-06 11:36:07', '03.jpg', 1),
(7, 'asd', 'dsa', '2020-05-06 11:36:07', '02.jpg', 2),
(8, 'asd', 'dsadasd', '2020-05-06 11:36:07', '05.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `Post_Tag`
--

CREATE TABLE `Post_Tag` (
  `id_post` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16le;

--
-- Dumping data for table `Post_Tag`
--

INSERT INTO `Post_Tag` (`id_post`, `id_tag`) VALUES
(2, 0),
(2, 1),
(2, 3),
(2, 4),
(3, 2),
(7, 2);

-- --------------------------------------------------------

--
-- Table structure for table `Role`
--

CREATE TABLE `Role` (
  `id` int(11) NOT NULL,
  `role` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16le;

--
-- Dumping data for table `Role`
--

INSERT INTO `Role` (`id`, `role`) VALUES
(-1, 'Administrador'),
(0, 'Guest'),
(1, 'Utilizador');

-- --------------------------------------------------------

--
-- Table structure for table `Tag`
--

CREATE TABLE `Tag` (
  `id` int(11) NOT NULL,
  `tag` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16le;

--
-- Dumping data for table `Tag`
--

INSERT INTO `Tag` (`id`, `tag`) VALUES
(0, ' '),
(1, 'Biologia'),
(2, 'Quimica'),
(3, 'Informática'),
(4, 'Fisica');

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `id` int(11) NOT NULL,
  `email` varchar(64) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(512) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `img_path` varchar(512) NOT NULL DEFAULT '../no_image_available.jpg',
  `id_role` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf16le;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`id`, `email`, `username`, `password`, `created_date`, `img_path`, `id_role`) VALUES
(1, '', 'Anónimo', '', '2020-05-06 09:33:11', '../no_image_available.jpg', 1),
(2, 'tester@testermail.com', 'Tester', 'tester123', '2020-05-06 09:33:11', '../no_image_available.jpg', -1),
(3, 'funciona@funcionamail.com', 'Funciona', 'registo_funciona', '2020-05-06 09:33:11', '../no_image_available.jpg', 1),
(4, 'teste@aaa.com', 'teste', 'teste', '2020-05-06 09:33:11', '../no_image_available.jpg', 1),
(9, 'janette@janettemail.com', 'Janette', 'janette', '2020-05-06 13:09:32', '02.jpg', 1),
(14, 'anabelle@anabellemai.com', 'Anabelle', 'anabelle', '2020-05-07 15:16:39', '03.jpg', 1),
(17, 'mario@mariomail.com', 'Mario', 'Mario', '2020-05-07 15:16:39', '05.png', 1),
(18, 'janette@janettemail.com', 'qqq', 'teste', '2020-05-07 16:50:10', '../no_image_available.jpg', 1),
(19, 'fkasduh@jdfnsa.com', 'dsuaih', '1214', '2020-05-07 19:45:44', '../no_image_available.jpg', 1),
(20, 'a123@a.com', 'a123', 'a123', '2020-05-07 19:45:57', '../no_image_available.jpg', 1),
(21, 'andre@gmail.com', 'andre123', 'andre123', '2020-05-08 17:12:47', '../no_image_available.jpg', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Category`
--
ALTER TABLE `Category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Event`
--
ALTER TABLE `Event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Event_Post`
--
ALTER TABLE `Event_Post`
  ADD UNIQUE KEY `id_event_post` (`id_event`,`id_post`) USING BTREE;

--
-- Indexes for table `Event_Tag`
--
ALTER TABLE `Event_Tag`
  ADD UNIQUE KEY `event_id` (`event_id`,`tag_id`);

--
-- Indexes for table `Message`
--
ALTER TABLE `Message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `Post`
--
ALTER TABLE `Post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Post_Tag`
--
ALTER TABLE `Post_Tag`
  ADD UNIQUE KEY `id_post` (`id_post`,`id_tag`);

--
-- Indexes for table `Role`
--
ALTER TABLE `Role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Tag`
--
ALTER TABLE `Tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Category`
--
ALTER TABLE `Category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `Event`
--
ALTER TABLE `Event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `Message`
--
ALTER TABLE `Message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `Post`
--
ALTER TABLE `Post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `Role`
--
ALTER TABLE `Role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `Tag`
--
ALTER TABLE `Tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
