<?php
echo '
    <!-- ======= Most Recent Events Section ======= -->
    <section id="recent" class="section-with-bg wow fadeInUp">

      <div class="container">
        <div class="section-header">
          <h2>Most Recent</h2>
          <p>Our most recent events</p>
        </div>
        <div class="row">
    ';
            require 'database/public/get_most_recent_events.php';
echo '

        </div>
      </div>

    </section><!-- End Most Recents Section -->
';
