<?php
	session_start();

	echo '
	<!DOCTYPE html>
	<html lang="en" style="background-image: url(../../../../assets/img/gallery/azores.jpg); -background-size: cover; 
	background-position: center;  background-repeat: no-repeat;
"> 
	<head>
	<!-- Template Main CSS File -->
	<link rel="stylesheet" type="text/css" href="style.css">
	</head>
	<body>
	<!-- ======= Header ======= -->
	<header id="header">
		<div class="container">
		<div id="logo" class="pull-left">
		
			<a href="../../../../" class="scrollto">
			<img style="max-width: 250px;" src="../../../../assets/img/logo_2.png" alt="" title=""></a>
		</div>
		</div>
	</header><!-- End Header -->
	<table class="center">
	<tr><th>Thumbnail</th> <th>Titulo</th> <th>Descrição</th> </tr>
	';

	require '../../database_service.php';
	require '../../class/event.php';

	$evento=null;
	$tags=null;
	if( ! empty($_POST['keyword'])){
		$keyword=$_POST['keyword'];
		try {
			$sql = "SELECT Event.id AS id_event, Event.img_path, title, text, Event.created_date, start_date, end_date, category, visible, username 
					FROM Event
					LEFT JOIN Category ON Event.id_category=Category.id
					LEFT JOIN User ON Event.id_user=User.id
					WHERE (Event.title LIKE '%$keyword%' OR Event.text LIKE '%$keyword%') AND Event.visible=1 ORDER BY Event.created_date DESC";

			$stmt = $conn->prepare($sql);
			$stmt->execute();

			// set the resulting array to associative
			$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
			foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {
				$evento = new Event($v['id_event'], $v['title'], $v['text'], $v['created_date'], $v['start_date'], $v['end_date'], $v['category'], $tags, $v['visible'], $v['username'], $v['img_path']);
				echo '<tr>
						<td>
							<a href="../../public/event/event_details_form.php?id='.$v['id_event'].'"> 
								<img style="max-width: 150px;" src="../../../../assets/img/event/'.$evento->get_image().'">
							</a>
						</td>
						<td>
							<a href="../../public/event/event_details_form.php?id='.$v["id_event"].'">'.$evento->get_title().'</a>
						</td>
						<td>'.$evento->get_text().'</td>
						</tr>';
			}
		}	catch(PDOException $e)	{
			echo '<h2>Erro! Não foram encontrados resultados para o termo: "'.$keyword.'"</h2>';
			echo $sql . "<br>" . $e->getMessage();
		}
	}
	$conn=null;

echo '
</table>
</body>
<footer>  
</footer>

</html>
';

?> 
