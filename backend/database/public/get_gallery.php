<?php 

require 'backend/database/database_service.php'; 
 
try {
    $stmt = $conn->prepare("SELECT id, img_path FROM Event WHERE (visible=1 AND img_path NOT LIKE '../no_image_available.jpg') ORDER BY Event.created_date DESC LIMIT 10");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {
        echo '<a href="backend/database/public/event/event_details_form.php?id='.$v['id'].'" class="venobox" data-gall="gallery-carousel"><img style="max-height: 250px;" src="assets/img/event/'.$v['img_path'].'"></a>
        ';
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
?>
