<?php
 require 'backend/database/database_service.php'; 

// echo $month;
// if($month==0){ $month=12; }
 try {
    $stmt = $conn->prepare("SELECT Event.id, title, text, Event.img_path, Event.created_date, username 
                            FROM Event LEFT JOIN User ON User.id=Event.id ORDER BY Event.created_date DESC LIMIT 10");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {

echo '

             <div class="row schedule-item">
              <div class="col-md-2"><time>'.$v['created_date'].'</time></div>
              <div class="col-md-10">
                <div class="trending">
                  <a  href="backend/database/public/event/event_details_form.php?id='.$v['id'].'"><img src="assets/img/event/'.$v['img_path'].'"></a>
                </div>
                <h4><a href="backend/database/public/event/event_details_form.php?id='.$v['id'].'" >'.$v["title"].'</a> <span>@'.$v['username'].'</span></h4>
                <p>'.$v['text'].'</p>
                </div>';
            
            try {
                $query2 = $conn->prepare("SELECT DISTINCT tag 
                                        FROM Tag LEFT JOIN Event_Tag ON Tag.id=Event_Tag.tag_id 
                                        WHERE Event_Tag.event_id=".$v['id']." ORDER BY Tag.tag ASC LIMIT 8");
                $query2->execute();

                // set the resulting array to associative
                $result_2 = $query2->setFetchMode(PDO::FETCH_ASSOC);
                $size=$query2->rowCount();
                if($size>0){
                    echo '<div><p>Tags: ';

                    foreach(new RecursiveArrayIterator($query2->fetchAll()) as $l=>$u) {
                        echo '<span>'.$u['tag'].'</span> ';
                    }
                    echo '</p></div>';
                }

            }
            catch(PDOException $e) {
                echo "Error: " . $e->getMessage();
            }
    echo '</div>';
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;


?>
