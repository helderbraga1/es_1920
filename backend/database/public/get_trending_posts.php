 <?php 
// echo getcwd();
 require 'backend/database/database_service.php'; 
 
try {
    $stmt=$conn->prepare("SELECT id_event, COUNT(*)
        FROM Event_Post
        GROUP BY id_event
        ORDER BY id_event DESC LIMIT 6");

    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {
        try {
            $stmt2=$conn->prepare("SELECT img_path, title 
                                FROM Event
                                WHERE id=".$v['id_event']."
                                LIMIT 1");
            $stmt2->execute();

            // set the resulting array to associative
            $result2 = $stmt2->setFetchMode(PDO::FETCH_ASSOC);
            foreach(new RecursiveArrayIterator($stmt2->fetchAll()) as $l=>$u) {
                echo '
                    <div class="col-lg-4 col-md-6">
                    <div class="trending">
                    <a href="backend/database/public/event/event_details_form.php?id='.$v['id_event'].'"><img href="backend/database/public/event/event_details_form.php?id='.$v['id_event'].'" src="assets/img/event/'.$u['img_path'].'"" class="img-fluid"></a>
                    <div class="details">
                        <h3><a href="backend/database/public/event/event_details_form.php?id='.$v['id_event'].'">'.$u['title'].'</a></h3><br><br>
                    </div>
                    </div>
                </div>
                ';
            }
        }
        catch(PDOException $e) { echo "Error: " . $e->getMessage(); }
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
?>
 
