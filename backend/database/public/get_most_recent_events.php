 <?php 
// echo getcwd();
 require 'backend/database/database_service.php'; 
 
try {
    $stmt = $conn->prepare("SELECT Event.id, title, text, img_path FROM Event WHERE visible=1 ORDER BY Event.created_date DESC LIMIT 3");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {
        echo '
          <div class="col-lg-4 col-md-6">
            <div class="hotel">
              <div class="hotel-img">
                <a href="backend/database/public/event/event_details_form.php?id='.$v['id'].'" ><img src="assets/img/event/'.$v['img_path'].'" class="img-fluid" value=></a>
              </div>
              <div>
                <h3><a href="backend/database/public/event/event_details_form.php?id='.$v['id'].'">'.$v['title'].'</a></h3>
                <p>'.$v['text'].'</p>
              </div>
            </div>
          </div>
        ';
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
?>
