<?php
 require 'backend/database/database_service.php'; 

 try {
    $stmt = $conn->prepare("SELECT Post.id, title, text, Post.img_path, Post.created_date, username FROM Post LEFT JOIN User ON User.id=Post.id_user ORDER BY Post.created_date DESC LIMIT 10");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {

echo '
             <div class="row schedule-item">
              <div class="col-md-2"><time>'.$v['created_date'].'</time></div>
              <div class="col-md-10">
                <div class="trending">
                  <img src="assets/img/user/'.$v['img_path'].'">
                </div>
                <h4>'.$v["title"].' <span>@'.$v['username'].'</span>';



    echo '    </h4>
            <p>'.$v['text'].'</p>
                </div>';
            
            try {
                $query2 = $conn->prepare("SELECT DISTINCT tag FROM Tag LEFT JOIN Post_Tag ON Tag.id=Post_Tag.id_tag WHERE (Post_Tag.id_post=".$v['id'].") ORDER BY Tag.tag ASC LIMIT 8");
                $query2->execute();

                // set the resulting array to associative
                $result_2 = $query2->setFetchMode(PDO::FETCH_ASSOC);
                $size=$query2->rowCount();
                if($size>0){
                    echo '<div><p>Tags: ';

                    foreach(new RecursiveArrayIterator($query2->fetchAll()) as $l=>$u) {
                        echo '<span style="height: 25px;
                        width: 75px;
                        background-color: #2a289c;
                        border-radius: 50%;    
                        color: aliceblue;

                        display: inline-block;">'.$u['tag'].'</span> ';
                    }
                    echo '</p></div>';
                }

            }
            catch(PDOException $e) {
                echo "Error: " . $e->getMessage();
            }
    echo '</div>';
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;

// echo getcwd() $v['title']

?>
