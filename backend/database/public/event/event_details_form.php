<?php
session_start();
echo '
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Event Details</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="../../../../assets/img/favicon.png" rel="icon">
  <link href="../../../../assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="../../../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../../../assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="../../../../assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="../../../../assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="../../../../assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="../../../../assets/css/style.css" rel="stylesheet">


</head>

<body>
    
  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">
      <div id="logo" class="pull-left">
       
        <a href="../../../../" class="scrollto"><img src="../../../../assets/img/logo_2.png" alt="" title=""></a>
      </div>
      <div>
      <nav id="nav-menu-container">
        <ul class="nav-menu">
          ';
        if($_SESSION['username'] != 'Anonimo')
        {
            echo "<li>".$_SESSION['role']." </li>"."<li>@<i>".$_SESSION['username']."</i></li>";
        }
        echo '

          </nav><!-- #nav-menu-container -->
      </div>
    </div>
  </header><!-- End Header -->

  <main id="main" class="main-page">

    <!-- ======= Event Details Section ======= -->
    <section id="speakers-details" class="wow fadeIn">
      <div class="container">';
require 'get_event.php';
echo '
      </div>

    </section>

  </main><!-- End #main -->

   <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>FOLLOW US ON SOCIAL MEDIA</h4>
             <div class="social-links">
             <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
              <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
              <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
              <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
            </div>
              
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>NAVIGATE</h4>
            <ul>
              <li><i class="fa fa-angle-right"></i> <a href="#">About us</a></li>
              <li><i class="fa fa-angle-right"></i> <a href="#">Sitemap</a></li>
              <li><i class="fa fa-angle-right"></i> <a href="#">Support</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
                <img src="../../../../assets/img/logo1.png" class="img-fluid">
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>UAç</strong>. All Rights Reserved
      </div>
      <div class="credits">
        Designed by Group 4
      </div>
    </div>
  </footer><!-- End  Footer -->
  
  <a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="../../../../assets/vendor/jquery/jquery.min.js"></script>
  <script src="../../../../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../../../../assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="../../../../assets/vendor/php-email-form/validate.js"></script>
  <script src="../../../../assets/vendor/wow/wow.min.js"></script>
  <script src="../../../../assets/vendor/venobox/venobox.min.js"></script>
  <script src="../../../../assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="../../../../assets/vendor/superfish/superfish.min.js"></script>
  <script src="../../../../assets/vendor/hoverIntent/hoverIntent.js"></script>

  <!-- Template Main JS File -->
  <script src="../../../../assets/js/main.js"></script>

</body>

</html>
';?>