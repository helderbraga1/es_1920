<?php 
 require '../../database_service.php'; 
 $id=$_GET['id'];
try {
    $stmt = $conn->prepare("SELECT Event.id, title, start_date, end_date, text, img_path FROM Event WHERE visible=1 AND Event.id=".$id." LIMIT 1");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {
        echo '
        <div class="section-header">
          <h2>Event details</h2>
        </div>

        <div class="row">
          <div class="col-md-5">
            <img src="../../../../assets/img/event/'.$v['img_path'].'" class="img-fluid">
            <br>
            <br>
            <br>
          </div>

          <div class="col-md-6">
            <div class="details">
              <h2>'.$v['title'].'</h2>
              <div class="social">
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-google-plus"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
              </div>
              <p>'.$v['text'].'</p>
              
              <p>Data de inicio: '.$v['start_date'].'</p>
              <p>Data do fim: '.$v['end_date'].'</p>
              ';
              if($_SESSION['id_role'] == 1){
              echo '
              <p> Quer assistir esse evento? <button><i class="fa fa-thumbs-up"></i></button>   <button><i class="fa fa-thumbs-down"> </i></button> </p>
              ';
              }
echo '
            </div>
          </div>

        </div>

        ';
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
?>
