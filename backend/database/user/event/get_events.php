 <?php 

 require "../database/database_service.php"; 
// session_start();
try {
    $stmt = $conn->prepare("SELECT Event.id, Event.img_path, title, text, Event.created_date, start_date, end_date, username FROM Event LEFT JOIN User ON Event.id_user=User.id LIMIT 20");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {
    //print_r($v);
    //echo '<br>';
        echo '
        <tr>
            <td>'.$v['id'].'</td>
            <td colspan="1"><img style="border-radius: 25%;max-width:200px;max-height:200px;" src="../../assets/img/event/'.$v['img_path'].'"/></td>
            ';
            if($_SESSION['id_role'] == -1) {
                
            echo '
            <form action="../database/user/event/edit/edit.php" method="post">
            ';
        }

            echo '
            <td colspan="1"><input name="event_title'.$v['id'].'" value="'.$v['title'].'"></td>
            <td colspan="1"><input name="event_text'.$v['id'].'" value="'.$v['text'].'"></td>
            <td colspan="1"><input type="date" value="'.$v['created_date'].'"></td>
            <td colspan="1"><input name="event_start_date'.$v['id'].'" type="date" value="'.$v['start_date'].'"></td>
            <td colspan="1"><input name="event_end_date'.$v['id'].'" type="date" value="'.$v['end_date'].'"></td>
            <td colspan="1"><i>@'.$v['username'].'</i></td>
            ';

            if($_SESSION['id_role'] == -1) {
                echo '
            <td colspan="1">                
                
                    <button name="event_id" type="submit" value='.$v['id'].' class="material-icons button edit">edit</button>
                    </form>   
                </td>
            <td colspan="1">
                <form action="../database/user/event/delete/delete_event_by_id.php" method="post">
                    <button name="event_id" type="submit" value='.$v["id"].' class="material-icons button delete">delete</button>
                </form>
            </td>';
            }
            echo '</tr>';
        }

        if($_SESSION['id_role'] == -1) {
        echo '

        <tr>
            <form action="../database/user/admin/event/add/add.php" method="post">

            <td></td>
            <td colspan="1">
                <input name="event_img_path" type="file">
            </td>
            <td colspan="1"><input placeholder="Title" name="event_title"></td>
            <td colspan="1"><input placeholder="Text" name="event_text"></td>
            <td colspan="1"></td>
            <td colspan="1"><input type="date" name="event_start_date"></td>
            <td colspan="1"><input type="date" name="event_end_date"></td>
            <td colspan="1"><i name="event_img_path"></i></td>
            <td colspan="1"></td>                ';
                echo '
            <td colspan="1"><button name="event_id" type="submit" value='.$v['id'].' class="material-icons button add">Add</button>
            </td>
            <input type="hidden" name="event_user_id" value="'.$_SESSION['id_user'].'">            
            </form>

        </tr>
        ';
    
        }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
?>