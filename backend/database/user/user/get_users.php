 <?php 

 require "../database/database_service.php"; 
 
try {
    $stmt = $conn->prepare("SELECT User.id, img_path, email, username, role FROM User LEFT JOIN Role ON User.id_role=Role.id LIMIT 20");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {
    //print_r($v);
    //echo '<br>';
        echo '
        <tr>
            <td>'.$v['id'].'</td>
            <td colspan="2"><img style="border-radius: 25%;max-width:100%;max-height:80%;" src="../../assets/img/user/'.$v['img_path'].'"/></td>
            <td colspan="2"><input value="'.$v['username'].'"></td>
            <td colspan="6"><input value="'.$v['email'].'"></td>
            <td colspan="2">'.$v['role'].'</td>
            <td colspan="2">                
                <form action="../database/user/user/edit/edit.php" method="post">
                    <button name="user_id" type="submit" value='.$v['id'].' class="material-icons button delete">edit</button>
                </form></td>
            <td colspan="2">
                <form action="../database/user/user/delete/delete.php" method="post">
                    <button name="user_id" type="submit" value='.$v['id'].' class="material-icons button delete">delete</button>
                </form>
            </td>
        </tr>

        ';
    }

}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
?>
 
