<?php echo '
      <div class="container">
        <div class="section-header">
          <h2>Create Tag</h2>
        </div>

        <div class="edit-content">
            <div class="form-group">
                <input type="text" class="form-input" name="create-tag" id="create-tag" placeholder="Name of tag"/>
            </div>
            <div class="form-group">
            <input type="submit" name="submit" id="create-submit" class="form-submit" value="Create"/>
            </div>
          </div>
      </div>

</body>
</html>
';