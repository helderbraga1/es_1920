 <?php 

 require "../database/database_service.php"; 
 
try {
    $stmt = $conn->prepare("SELECT id, tag FROM Tag ORDER BY tag ASC LIMIT 10");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {
        echo '
        <tr>
            <td>'.$v['id'].'</td>
            <td colspan="2"><input type="text" value="'.$v['tag'].'"></td>
            <td colspan="2">                
                <form action="../database/user/tag/edit/edit_tag_by_id.php" method="post">
                    <button name="tag_id" type="submit" value='.$v['id'].' class="material-icons button delete">edit</button>
                </form></td>
            <td colspan="2">
                <form action="../database/user/admin/tag/delete/delete.php" method="post">
                    <button name="tag_id" type="submit" value='.$v['id'].' class="material-icons button delete">delete</button>
                </form>
            </td>
        </tr>

        ';
    }

}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
?>
 
