 <?php 

 require "../database/database_service.php"; 
 
try {
    $stmt = $conn->prepare("SELECT id, category FROM Category ORDER BY category ASC LIMIT 10");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {
        echo '
        <tr>
            <td>'.$v['id'].'</td>
            <td colspan="2"><input type="text" value="'.$v['category'].'"></td>
            <td colspan="2">                
                <form action="../database/user/category/edit/edit_category_by_id.php" method="post">
                    <button name="category_id" type="submit" value='.$v['id'].' class="material-icons button delete">edit</button>
                </form></td>
            <td colspan="2">
                <form action="../database/user/category/delete/delete_category_by_id.php" method="post">
                    <button name="category_id" type="submit" value='.$v['id'].' class="material-icons button delete">delete</button>
                </form>
            </td>
        </tr>

        ';
    }

}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
?>
 
