 <?php 

 require "../database/database_service.php"; 
// session_start();
try {
    $stmt = $conn->prepare("SELECT Post.id, Post.title, Post.text, Post.created_date, User.img_path, User.username 
                            FROM Post
                            LEFT JOIN User ON (User.id=Post.id_user) LIMIT 20");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {
    //print_r($v);
    //echo '<br>';
        echo '
        <tr>
            <td>'.$v['id'].'</td>
            <td colspan="1"><img style="border-radius: 25%;max-width:50%;max-height:60%;" src="../../assets/img/user/'.$v['img_path'].'"/></td>
            ';
            if($_SESSION['id_role'] == -1) {
                
            echo '
            <form action="../database/user/post/edit/edit.php" method="post">
            ';
        }

            echo '
            <td colspan="1"><input name="post_title'.$v['id'].'" value="'.$v['title'].'"></td>
            <td colspan="1"><input name="post_text'.$v['id'].'" value="'.$v['text'].'"></td>
            <td colspan="1"><input type="date" value="'.$v['created_date'].'"></td>
            <td colspan="1"></td>
            <td colspan="1"></td>
            ';
//            <td colspan="1"><img name="event_image'.$v['id'].'" src="'.$v['event_image'].'"></td>
//            <td colspan="1"><input name="event_title'.$v['id'].'" type="text" value="'.$v['event_title'].'"></td>
            echo '
            <td colspan="1"><i>@'.$v['username'].'</i></td>
            ';

            if($_SESSION['id_role'] == -1) {
                echo '
            <td colspan="1">                
                
                    <button name="post_id" type="submit" value='.$v['id'].' class="material-icons button edit">edit</button>
                    </form>   
                </td>
            <td colspan="1">
                <form action="../database/user/admin/post/delete/delete.php" method="post">
                    <button name="post_id" type="submit" value='.$v["id"].' class="material-icons button delete">delete</button>
                </form>
            </td>';
            }
            echo '</tr>';
        }

        if($_SESSION['id_role'] == -1) {
        echo '

        <tr>
            <form action="../database/user/admin/post/add/add.php" method="post">

            <td></td>
            <td colspan="1">
                <input name="post_img_path" type="file">
            </td>
            <td colspan="1"><input placeholder="Title" name="post_title"></td>
            <td colspan="1"><input placeholder="Text" name="post_text"></td>
            <td colspan="1"></td>
            <td colspan="1"><i name="event_img_path"></i></td>
            <td colspan="1"></td>
            <td colspan="1"></td>                ';
                echo '
            <td colspan="1"><button name="event_id" type="submit" value='.$v['id'].' class="material-icons button add">Add</button>
            </td>
            <td colspan="1"></td>
            <input type="hidden" name="post_user_id" value="'.$_SESSION['id_user'].'">            
            </form>

        </tr>
        ';
    
        }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
?>