<?php
/*  Class responsible by the "Contact US" form segment of the page */
class Message {

    //Atributes
    private $id;
    private $name;
    private $email;
    private $title;
    private $text;
    private $created_date;
    private $id_user;

    //Constructor
    public function __construct($id, $name, $email, $title, $text, $created_date, $id_user)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->title = $title;
        $this->text = $text;
        $this->created_date = $created_date;
        $this->id_user = $id_user;
    }

    //Getters
    public function get_id()
    {
        return $this->id;
    }
    public function get_name()
    {
        return $this->name;
    }
    public function get_email()
    {
        return $this->email;
    }

    public function get_title()
    {
        return $this->title;
    }

    public function get_text()
    {
        return $this->text;
    }

    public function get_created_date()
    {
        return $this->created_date;
    }

    public function get_id_user()
    {
        return $this->id_user;
    }

    //Setters
    public function set_id($id)
    {
        $this->id=$id;
    }
    public function set_name($name)
    {
        $this->name = $name;
    }
    public function set_email($email)
    {
        $this->email = $email;
    }

    public function set_title($title)
    {
        $this->title = $title;
    }

    public function set_text($text)
    {
        $this->text = $text;
    }

    public function set_created_date($created_date)
    {
        $this->created_date = $created_date;
    }

    public function set_id_user($id_user)
    {
        $this->id_user = $id_user;
    }

}

?> 
 
 
