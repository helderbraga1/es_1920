<?php

class Event {

    private $id;
    private $title;
    private $text;
    private $created_date;
    private $start_date;
    private $end_date;
    private $id_category;
    private $tags;
    private $visible;
    private $id_user;
    private $image;

    public function __construct($id, $title, $text, $created_date, $start_date, $end_date, $id_category, $tags, $visible, $id_user, $img)
    {
        $this->id = $id;
        $this->title = $title;
        $this->text = $text;
        $this->created_date = $created_date;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->tags = $tags;
        $this->id_category = $id_category;
        $this->visible = $visible;
        $this->user = $id_user;
        $this->image = $img;
    }

    public function get_id()
    {
        return $this->id;
    }

    public function get_title()
    {
        return $this->title;
    }

    public function get_text()
    {
        return $this->text;
    }

    public function get_created_date()
    {
        return $this->created_date;
    }

    public function get_start_date()
    {
        return $this->start_date;
    }

    public function get_end_date()
    {
        return $this->end_date;
    }

    public function get_id_category()
    {
        return $this->id_category;
    }

    public function get_tags()
    {
        return $this->tags;
    }

    public function get_visible()
    {
        return $this->visible;
    }

    public function get_id_user()
    {
        return $this->id_user;
    }

    public function get_image()
    {
        return $this->image;
    }


    public function set_id()
    {
        return $this->id;
    }

    public function set_title()
    {
        return $this->title;
    }

    public function set_text()
    {
        return $this->text;
    }

    public function set_created_date()
    {
        return $this->created_date;
    }

    public function set_end_date()
    {
        return $this->end_date;
    }

    public function set_id_category($id_category)
    {
        return $this->id_category=$id_category;
    }

    public function set_id_tag()
    {
        return $this->id_tag;
    }

    public function set_visible()
    {
        return $this->visible;
    }

    public function set_id_user()
    {
        return $this->id_user;
    }

}

?> 
 
