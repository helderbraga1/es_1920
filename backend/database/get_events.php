<?php
	require 'class/event.php';
    require 'connection.php';

	try {
		$sql = "SELECT * FROM events, tags, users ORDER BY created_date DESC";

		$stmt = $conn->prepare($sql);
		$stmt->execute();

		// set the resulting array to associative
		$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
		//echo '<select id="id_evento" name="id_evento">';
		foreach(new RecursiveArrayIterator($stmt->fetchAll()) as $k=>$v) {
			$evento = new Event($v['id'], $v['title'], $v['text'], $v['created_date'], $v['start_date'], $v['end_date'], $v['tag'], $v['status'], $v['username']);
			print '<p>'.$evento->get_created_date().'</p>';
		}
		//echo '</select>';
	}	catch(PDOException $e)	{
		echo $sql . "<br>" . $e->getMessage();
	}
$conn=null;
?> 
