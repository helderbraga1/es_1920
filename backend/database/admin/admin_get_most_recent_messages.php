 <?php 
  require "backend/database/database_service.php"; 

echo "<table style='border: solid 1px black;'>";
echo "<tr><th>Id</th><th>Title</th><th>Text</th><th>Name</th><th>Date</th><th>Email</th><th>Username</th></tr>";

try {
    $stmt = $conn->prepare("SELECT Message.id, title, text, name, Message.created_date, Message.email, username FROM Message LEFT JOIN User ON Message.id_user=User.id ORDER BY Message.created_date DESC LIMIT 20");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach(new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) {
        echo $v;
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
echo "</table>";
?>
