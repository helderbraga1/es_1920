 <?php 
 require "backend/database/database_service.php"; 
 
echo "<table style='border: solid 1px black;'>";
echo "<tr><th>Id</th><th>Title</th><th>Text</th><th>Start Date</th><th>End Date</th><th>Username</th></tr>";

try {
    $stmt = $conn->prepare("SELECT Event.id, title, text, start_date, end_date, username FROM Event LEFT JOIN User ON Event.id_user=User.id ORDER BY end_date ASC LIMIT 5");
    $stmt->execute();

    // set the resulting array to associative
    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
    foreach(new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) {
        echo $v;
    }
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$conn = null;
echo "</table>";
?>
