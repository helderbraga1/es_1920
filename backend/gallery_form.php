<?php echo '
    <!-- ======= Gallery Section ======= -->
        <section id="gallery" class="wow fadeInUp">

        <div class="container">
            <div class="section-header">
            <h2>Gallery</h2>
            <p>Check our gallery from the recent events</p>
            </div>
        </div>

        <div class="owl-carousel gallery-carousel">
    ';
    require 'backend/database/public/get_gallery.php';
echo '
      </div>

    </section><!-- End Gallery Section -->
    
';
?>
