<?php
echo '
    <!-- ======= Posts Section ======= -->
    <section id="post" class="section-with-bg">
      <div class="container wow fadeInUp">
        <div class="section-header">
          <h2>Most Recent Posts</h2>
          <p>Here are our most recent interactions!</p>
        </div>

        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" href="#latest-1" role="tab" data-toggle="tab">Latest</a>
          </li>
        </ul>

        <div class="tab-content row justify-content-center">

          <div role="tabpanel" class="col-lg-9 tab-pane fade show active" id="latest-1">
';

          require 'backend/database/public/get_most_recent_posts.php';
          
echo '            
          </div>
        </div>
      </div>

    </section><!-- End Posts Section -->
'; ?>
