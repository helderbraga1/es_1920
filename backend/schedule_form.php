<?php
echo '
    <!-- ======= Schedule Section ======= -->
    <section id="schedule" class="section-with-bg">
      <div class="container wow fadeInUp">
        <div class="section-header">
          <h2>Monthly Highlights</h2>
          <p>Here is our montlhy schedule</p>
        </div>

        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" href="#day-1" role="tab" data-toggle="tab">April</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#day-2" role="tab" data-toggle="tab">May</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#day-3" role="tab" data-toggle="tab">June</a>
          </li>
        </ul>


        <div class="tab-content row justify-content-center">

          <!-- Schedule Month 1 -->
          <div role="tabpanel" class="col-lg-9 tab-pane fade show active" id="day-1">
';
            //$month = date('n'); //Get current month

            require 'database/public/get_schedule.php';
echo '
          </div>
          <!-- End Schdule Month 1 -->

          <!-- Schdule Month 2 -->
          <div role="tabpanel" class="col-lg-9  tab-pane fade" id="day-2">

             <div class="row schedule-item">
              <div class="col-md-2"><time>DATE</time></div>
              <div class="col-md-10">
                <div class="trending">
                  <img src="assets/img/branco.png" alt="">
                </div>
                <h4>Name of the Event<span>Hours</span></h4>
                <p>Little descriptison</p>
              </div>
            </div>

          </div>
          <!-- End Schdule Month 2 -->

          <!-- Schdule Month 3 -->
          <div role="tabpanel" class="col-lg-9  tab-pane fade" id="day-3">

             <div class="row schedule-item">
              <div class="col-md-2"><time>DATE</time></div>
              <div class="col-md-10">
                <div class="trending">
                  <img src="assets/img/branco.png" alt="">
                </div>
                <h4>Name of the Event<span>Hours</span></h4>
                <p>Little descriptison</p>
              </div>
            </div>

          </div>
          <!-- End Schdule Month 3 -->

        </div>

      </div>

    </section><!-- End Schedule Section -->
'; ?>
