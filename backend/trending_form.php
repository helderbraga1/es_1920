<?php
echo ' 
    <!-- ======= Trending Section ======= -->
    <section id="trending" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Trending</h2>
          <p>Here are the events with more user interactions</p>
        </div>

        <div class="row">';
          require 'backend/database/public/get_trending_posts.php';
echo '   
        </div>
      </div>

    </section><!-- End Speakers Section -->
    ';
?>
