<?php 
echo '
    <!-- ======= Admin Section ======= -->
    <section id="admin" class="wow fadeInUp">

      <div class="container">
        <div class="section-header">
          <h2>Admin</h2>
          <h3>User Roles</h3>
';

include 'database/admin/admin_get_users.php';

echo '<br>
<h3>Events Ending Soon</h3>
';

include 'database/admin/admin_get_most_recent_events.php';

echo '<br>
<h3>Most Recent Messages</h3>
';

include 'database/admin/admin_get_most_recent_messages.php';
echo '

        </div>
      </div>

      <div class="owl-carousel gallery-carousel">
        <a href="assets/img/gallery/8.jpg" class="venobox" data-gall="gallery-carousel"><img src="assets/img/gallery/8.jpg" alt=""></a>

      </div>

    </section><!-- End Admin Section -->

';
