<?php 
session_start();
echo '
<!DOCTYPE html>
<html lang="en" "> 
<head>
<script src="../../assets/vendor/jquery/jquery.min.js"></script>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

  <link href="test.css" rel="stylesheet">
  <link href="table.css" rel="stylesheet">

  <!-- Favicons -->
  <link href="../../assets/img/favicon.png" rel="icon">
  <link href="../../assets/img/apple-touch-icon.png" rel="apple-touch-icon">
  <script src="https://kit.fontawesome.com/d7c5e79122.js" crossorigin="anonymous"></script>

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="../../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../../assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="../../assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="../../assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="../../assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  
  
  <!-- Template Main CSS File -->
  <link href="../../assets/css/style.css" rel="stylesheet">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  
</head>
<body>
  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">
      <div id="logo" class="pull-left">
       
        <a href="../../index.php" class="scrollto"><img src="../../assets/img/logo_2.png" alt="" title=""></a>
      </div>
    </div>
  </header><!-- End Header -->
<br><br><br><br><br>
<section>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">



<div class="container">
    <div class="login-signup">
      <div class="row">
        <div class="col-sm-10 nav-tab-holder">
        <ul class="nav nav-tabs row" role="tablist">
        ';
        if($_SESSION['id_role'] == -1) {
        echo '
          <li role="presentation" class="col-sm-4"><a href="#user" aria-controls="user" role="tab" data-toggle="tab" id="changecolor">Category</a></li>
          
          ';
        }
        echo '  <li role="presentation" class="active col-sm-4"><a href="#event" aria-controls="event" role="tab" data-toggle="tab" id="changecolor">Event</a></li>
         '; 
        if($_SESSION['id_role'] == -1) {
          echo '
          <li role="presentation" class="col-sm-4"><a href="#post" aria-controls="post" role="tab" data-toggle="tab" id="changecolor">Post</a></li>
          <li role="presentation" class="col-sm-4"><a href="#tag" aria-controls="tag" role="tab" data-toggle="tab" id="changecolor">Tag</a></li>
          <li role="presentation" class="col-sm-4"><a href="#category" aria-controls="category" role="tab" data-toggle="tab" id="changecolor">User</a></li>
          ';
        }

          echo '
        </ul>
      </div>
      <!-- USER SECTION -->

      <div class="tab-content">
      <div role="tabpanel" class="tab-pane" id="category">
          <div class="row">
              <div class="col-sm-10 mobile-pull">

                  <h3 class="active">User</h3>
                  <form action="backend\database\user\admin\user\add\add.php" method="post">
                      <table>
                          <thead>
                              <tr>
                                  <th colspan="10">Users</th>
                              </tr>

                              <tr>
                                  <th>#</th>
                                  <th colspan="1">Image</th>
                                  <th colspan="1">Username</th>
                                  <th colspan="1">Email</th>
                                  <th colspan="1">Role</th>

                                  <th colspan="1">Edit</th>
                                  <th colspan="1">Delete</th>
                              </tr>
                          </thead>

                          <tbody>
                            ';
                            require('../database/user/user/get_users.php');
                            echo '
                          </tbody>
                      </table>
                  </form>
              </div>
          </div>
      </div>
      <!-- END USER SECTION -->

      <!-- EVENT SECTION -->

      <div role="tabpanel" class="tab-pane active" id="event">
          <div class="row">
              <div class="col-sm-10 mobile-pull">

                  <h3 id="event" class="active">Event</h3>
                  <form action="index.html" method="post">
                      <table>
                          <thead>
                              <tr>
                                  <th colspan="10">Events</th>
                              </tr>

                              <tr>
                                  <th>#</th>
                                  <th colspan="1">Image</th>
                                  <th colspan="1">Title</th>
                                  <th colspan="1">Text</th>
                                  <th colspan="1">Created</th>
                                  <th colspan="1">Start</th>
                                  <th colspan="1">End</th>
                                  <th colspan="1">By</th>
                                  ';
                                  if($_SESSION['id_role'] == -1) {
                                  echo '
                                  <th colspan="1">Edit</th>
                                  <th colspan="1">Delete</th>
                                  ';}
                                  echo '
                              </tr>
                          </thead>

                          <tbody> 
                            ';
                            require('../database/user/event/get_events.php');
                            echo '
                          </tbody>
                      </table>
                  </form>
              </div>
          </div>
      </div>
      <!-- END EVENT SECTION -->

      <!-- POST SECTION -->

      <div role="tabpanel" class="tab-pane" id="post">
          <div class="row">
              <div class="col-sm-10 mobile-pull">

                  <h3 id="event">Post</h3>
                  <form action="index.html" method="post">
                      <table>
                          <thead>
                              <tr>
                                  <th colspan="10">Posts</th>
                              </tr>

                              <tr>
                                  <th>#</th>
                                  <th colspan="1">Image</th>
                                  <th colspan="1">Title</th>
                                  <th colspan="1">Text</th>
                                  <th colspan="1">Created</th>
                                  <th colspan="1">Event Image</th>
                                  <th colspan="1">Event Title</th>
                                  <th colspan="1">By</th>
                                  <th colspan="1">Edit</th>
                                  <th colspan="1">Delete</th>
                              </tr>
                          </thead>

                          <tbody>
                          ';
                            require('../database/user/post/get_posts.php'); 
                            echo '
                          </tbody>
                      </table>

                  </form>
              </div>

          </div>
          <!-- END POST SECTION -->

      </div>

      <!-- TAG SECTION -->
      <div role="tabpanel" class="tab-pane" id="tag">
          <div class="row">

              <div class="col-sm-6 mobile-pull">
                  <article role="login">
                      <h3 class="text-center"> Tag </h3>
                      <form class="signup" action="../../backend/database/user/admin/tag/add/add.php" method="post">
                          <div class="form-group">
                              <input name="tag" type="text" class="form-control" placeholder="Tag">
                          </div>

                          <div class="form-group">
                              <input type="submit" class="btn btn-success btn-block" value="SUBMIT">
                          </div>
                      </form>

                  </article>
              </div>

          </div>

          <div class="row">
              <div class="col-sm-10 mobile-pull">

                  <h3 id="category" class="active">Tag</h3>
                  <form action="index.html" method="post">
                      <table>
                          <thead>
                              <tr>
                                  <th colspan="10">Tag</th>
                              </tr>

                              <tr>
                                  <th>#</th>
                                  <th colspan="1">Tag</th>
                                  <th colspan="1">Edit</th>
                                  <th colspan="1">Delete</th>
                              </tr>
                          </thead>

                          <tbody>
                          ';
                            require('../database/user/tag/get_tags.php');
                            echo '
                          </tbody>
                      </table>

                  </form>
              </div>

          </div>
      </div>
      <!-- END TAG SECTION -->


      <!-- CATEGORY SECTION -->

      <div role="tabpanel" class="tab-pane" id="user">
          <div class="row">

              <div class="col-sm-6 mobile-pull">
                  <article role="login">
                      <h3 class="text-center"> Insert Category </h3>
                      <form class="signup" action="../../backend/database/user/admin/category/add/add.php" method="post">
                          <div class="form-group">
                              <input name="category" type="text" class="form-control" placeholder="Category">
                          </div>

                          <div class="form-group">
                              <input type="submit" class="btn btn-success btn-block" value="SUBMIT">
                          </div>
                      </form>
                  </article>
              </div>
          </div>

          <div class="row">
              <div class="col-sm-10 mobile-pull">
                  <h3 id="category" class="active">Show Categories</h3>
                  <form action="index.html" method="post">
                      <table>
                          <thead>
                              <tr>
                                  <th colspan="10">Category</th>
                              </tr>

                              <tr>
                                  <th>#</th>
                                  <th colspan="1">Category</th>
                                  <th colspan="1">Edit</th>
                                  <th colspan="1">Delete</th>
                              </tr>
                          </thead>

                          <tbody> ';
                            require('../database/user/category/get_categories.php');
                            echo '
                          </tbody>
                      </table>
                  </form>
              </div>
          </div>
      </div>
      <!-- CATEGORY SECTION -->

  </div>
</div>
</section>
</body>

</html>
';
?>